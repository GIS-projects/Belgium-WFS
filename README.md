
# WFS List Belgium
**A list of WFS services with data for Belgium**

## <a href="https://codeberg.org/gis-projects/Belgium-WFS">This repository has been migrated to Codeberg</a>

It can now be found on https://codeberg.org/gis-projects/Belgium-WFS