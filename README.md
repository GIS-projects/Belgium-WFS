
# WFS List Belgium
**A list of WFS services with data for Belgium**

The entire content of this repository is stored in this README.md document. All editing should be done on this file. The only other file is the LICENSE file.

More information about WFS Services kan be found on [Wikipedia](https://en.wikipedia.org/wiki/Web_Feature_Service).



More detailed information about all listed services can be found on [wfs.michelstuyts.be](https://wfs.michelstuyts.be).

An xml file to import all these services into [QGIS](https://qgis.org) can be downloaded from https://wfs.michelstuyts.be/qgis.php?lang=en.

## Belgium

* [CadastralLayers - FPS Finances - GAPD](https://ccff02.minfin.fgov.be/geoservices/arcgis/services/WMS/Cadastral_LayersWFS/MapServer/WFSServer) - [GetCapabilities](https://ccff02.minfin.fgov.be/geoservices/arcgis/services/WMS/Cadastral_LayersWFS/MapServer/WFSServer?REQUEST=GetCapabilities&SERVICE=WFS)

* [GeoServer Web Feature Service - geo.bipt-data.be](https://geo.bipt-data.be/geoserver/ows) - [GetCapabilities](https://geo.bipt-data.be/geoserver/ows?REQUEST=GetCapabilities&SERVICE=WFS)

* [GeoServer Web Feature Service - Biodiversity and Ecosystems Data and Information Centre (BEDIC)](https://spatial.naturalsciences.be/geoserver/ows) - [GetCapabilities](https://spatial.naturalsciences.be/geoserver/ows?REQUEST=GetCapabilities&SERVICE=WFS)

* [GeoServer Web Feature Service - Biodiversity and Ecosystems Data and Information Centre (BEDIC)](https://spatial.naturalsciences.be/geoserver/imsp/ows) - [GetCapabilities](https://spatial.naturalsciences.be/geoserver/imsp/ows?REQUEST=GetCapabilities&SERVICE=WFS)

* [IRCEL - CELINE - Web Feature Service - Belgian Interregional Environment Agency](https://geo.irceline.be/wfs) - [GetCapabilities](https://geo.irceline.be/wfs?REQUEST=GetCapabilities&SERVICE=WFS)

* [opendata.meteo.be - Royal Meteorological Institute of Belgium](https://opendata.meteo.be/service/wfs) - [GetCapabilities](https://opendata.meteo.be/service/wfs?REQUEST=GetCapabilities&SERVICE=WFS)

* [opendata.meteo.be - Royal Meteorological Institute of Belgium](https://opendata.meteo.be/service/lidar/wfs) - [GetCapabilities](https://opendata.meteo.be/service/lidar/wfs?REQUEST=GetCapabilities&SERVICE=WFS)

* [ROB Inspire GeoServer WFS - Royal Observatory of Belgium](https://inspire.seismology.be/geoserver/inspire/wfs) - [GetCapabilities](https://inspire.seismology.be/geoserver/inspire/wfs?REQUEST=GetCapabilities&SERVICE=WFS)

* [INSPIRE Download Service - FPS Finances - General Administration of Patrimonial Documentation (GAPD)](https://ccff02.minfin.fgov.be/geoservices/arcgis/rest/services/INSPIRE/AU/MapServer/exts/InspireFeatureDownload/service) - [GetCapabilities](https://ccff02.minfin.fgov.be/geoservices/arcgis/rest/services/INSPIRE/AU/MapServer/exts/InspireFeatureDownload/service?REQUEST=GetCapabilities&SERVICE=WFS)

* [INSPIRE Download Service - FPS Finances - General Administration of Patrimonial Documentation (GAPD)](https://ccff02.minfin.fgov.be/geoservices/arcgis/rest/services/INSPIRE/CP/MapServer/exts/InspireFeatureDownload/service) - [GetCapabilities](https://ccff02.minfin.fgov.be/geoservices/arcgis/rest/services/INSPIRE/CP/MapServer/exts/InspireFeatureDownload/service?REQUEST=GetCapabilities&SERVICE=WFS)

* [INSPIRE View service - CORINE Land Cover-2018-Belgium - National Geographic Institute](https://data.geo.be/ws/landcover/wfs) - [GetCapabilities](https://data.geo.be/ws/landcover/wfs?REQUEST=GetCapabilities&SERVICE=WFS)

* [National wastewater-based epidemiological surveillance of SARS-CoV-2 (WFS) - National Geographic Institute](https://data.geo.be/ws/sciensano/wfs) - [GetCapabilities](https://data.geo.be/ws/sciensano/wfs?REQUEST=GetCapabilities&SERVICE=WFS)

* [WFS - eservices.minfin.fgov.be](https://eservices.minfin.fgov.be/arcgis/services/R3C/Municipalities/MapServer/WFSServer) - [GetCapabilities](https://eservices.minfin.fgov.be/arcgis/services/R3C/Municipalities/MapServer/WFSServer?REQUEST=GetCapabilities&SERVICE=WFS)



## Brussels

* [Air, climat, énerie // Lucht, klimaat, energie - Bruxelles Environnement / Leefmilieu Brussel / Brussels Environment](https://ows.environnement.brussels/air) - [GetCapabilities](https://ows.environnement.brussels/air?REQUEST=GetCapabilities&SERVICE=WFS)

* [BE-LB WFS service - Bruxelles Environnement / Leefmilieu Brussel / Brussels Environment](https://wfs.environnement.brussels/belb) - [GetCapabilities](https://wfs.environnement.brussels/belb?REQUEST=GetCapabilities&SERVICE=WFS)

* [Bruenvi / Service de visualisation WFS - Bruxelles Environnement / Leefmilieu Brussel / Brussels Environment](https://ows.environnement.brussels/belb) - [GetCapabilities](https://ows.environnement.brussels/belb?REQUEST=GetCapabilities&SERVICE=WFS)

* [Bruenvi / Service de visualisation WMS - Nouvelle version - Bruxelles Environnement / Leefmilieu Brussel / Brussels Environment](https://wfs.environnement.brussels/belb_inspire) - [GetCapabilities](https://wfs.environnement.brussels/belb_inspire?REQUEST=GetCapabilities&SERVICE=WFS)

* [Bruit // Geluid - Bruxelles Environnement / Leefmilieu Brussel / Brussels Environment](https://ows.environnement.brussels/noise) - [GetCapabilities](https://ows.environnement.brussels/noise?REQUEST=GetCapabilities&SERVICE=WFS)

* [data-mobility.irisnet.be - Brussel Mobiliteit](https://data-mobility.irisnet.be/geoserver/bm_public_space/wfs) - [GetCapabilities](https://data-mobility.irisnet.be/geoserver/bm_public_space/wfs?REQUEST=GetCapabilities&SERVICE=WFS)

* [data-mobility.irisnet.be - Brussel Mobiliteit](https://data-mobility.irisnet.be/geoserver/bm_bike/wfs) - [GetCapabilities](https://data-mobility.irisnet.be/geoserver/bm_bike/wfs?REQUEST=GetCapabilities&SERVICE=WFS)

* [data-mobility.irisnet.be - Brussel Mobiliteit](https://data-mobility.irisnet.be/geoserver/bm_public_transport/wfs) - [GetCapabilities](https://data-mobility.irisnet.be/geoserver/bm_public_transport/wfs?REQUEST=GetCapabilities&SERVICE=WFS)

* [data-mobility.irisnet.be - Brussel Mobiliteit](https://data-mobility.irisnet.be/geoserver/bm_parking/wfs) - [GetCapabilities](https://data-mobility.irisnet.be/geoserver/bm_parking/wfs?REQUEST=GetCapabilities&SERVICE=WFS)

* [GeoServer Web Feature Service - urban.brussels](https://gis.urban.brussels/geoserver/ows) - [GetCapabilities](https://gis.urban.brussels/geoserver/ows?REQUEST=GetCapabilities&SERVICE=WFS)

* [GeoServer Web Feature Service - gis.urban.brussels](https://gis.urban.brussels/brugiservices/geoserverinspire/ows) - [GetCapabilities](https://gis.urban.brussels/brugiservices/geoserverinspire/ows?REQUEST=GetCapabilities&SERVICE=WFS)

* [geoservices-urbis.irisnet.be - Paradigm](https://geoservices-urbis.irisnet.be/geoserver/UrbisAdm/ows) - [GetCapabilities](https://geoservices-urbis.irisnet.be/geoserver/UrbisAdm/ows?REQUEST=GetCapabilities&SERVICE=WFS)

* [Sol et sous-sol // Bodem en ondergrond - Bruxelles Environnement / Leefmilieu Brussel / Brussels Environment](https://ows.environnement.brussels/soil) - [GetCapabilities](https://ows.environnement.brussels/soil?REQUEST=GetCapabilities&SERVICE=WFS)

* [WFS Brussel Mobiliteit - data-mobility.irisnet.be](https://data-mobility.irisnet.be/inspire/capabilities/nl/wfs) - [GetCapabilities](https://data-mobility.irisnet.be/inspire/capabilities/nl/wfs?REQUEST=GetCapabilities&SERVICE=WFS)

* [WFS Bruxelles Mobilite - data-mobility.irisnet.be](https://data-mobility.irisnet.be/inspire/capabilities/fr/wfs) - [GetCapabilities](https://data-mobility.irisnet.be/inspire/capabilities/fr/wfs?REQUEST=GetCapabilities&SERVICE=WFS)



## Flanders

* [Adressen - agentschap Digitaal Vlaanderen](https://geo.api.vlaanderen.be/ad/wfs) - [GetCapabilities](https://geo.api.vlaanderen.be/ad/wfs?REQUEST=GetCapabilities&SERVICE=WFS)

* [apps.energiesparen.be - Programma Earth Observation Data Science (EODaS), Agentschap Informatie Vlaanderen](https://apps.energiesparen.be/proxy/remote-sensing/geoserver/VEA/wfs) - [GetCapabilities](https://apps.energiesparen.be/proxy/remote-sensing/geoserver/VEA/wfs?REQUEST=GetCapabilities&SERVICE=WFS)

* [data.uitwisselingsplatform.be - data.uitwisselingsplatform.be](https://data.uitwisselingsplatform.be/be.dcjm.infrastructuur/datamerge-cji/cjmgeopunt/ows) - [GetCapabilities](https://data.uitwisselingsplatform.be/be.dcjm.infrastructuur/datamerge-cji/cjmgeopunt/ows?REQUEST=GetCapabilities&SERVICE=WFS)

* [DMOW Public WFS - Vlaamse overheid, Departement Mobiliteit en Openbare Werken](https://geoserver.gis.cloud.mow.vlaanderen.be/geoserver/wfs) - [GetCapabilities](https://geoserver.gis.cloud.mow.vlaanderen.be/geoserver/wfs?REQUEST=GetCapabilities&SERVICE=WFS)

* [Gebiedsbeheer/gebieden waar beperkingen gelden/gereguleerde gebieden en rapportage-eenheden - agentschap Digitaal Vlaanderen](https://geo.api.vlaanderen.be/am/wfs) - [GetCapabilities](https://geo.api.vlaanderen.be/am/wfs?REQUEST=GetCapabilities&SERVICE=WFS)

* [Gebouwen - agentschap Digitaal Vlaanderen](https://geo.api.vlaanderen.be/bu-core2d/wfs) - [GetCapabilities](https://geo.api.vlaanderen.be/bu-core2d/wfs?REQUEST=GetCapabilities&SERVICE=WFS)

* [geodata.toerismevlaanderen.be - Toerisme Vlaanderen](https://geodata.toerismevlaanderen.be/geoserver/wfs) - [GetCapabilities](https://geodata.toerismevlaanderen.be/geoserver/wfs?REQUEST=GetCapabilities&SERVICE=WFS)

* [GeoServer Web Feature Service - Gent](https://geo.gent.be/geoserver/ows) - [GetCapabilities](https://geo.gent.be/geoserver/ows?REQUEST=GetCapabilities&SERVICE=WFS)

* [GeoServer Web Feature Service - geo.onroerenderfgoed.be](https://geo.onroerenderfgoed.be/geoserver/wfs) - [GetCapabilities](https://geo.onroerenderfgoed.be/geoserver/wfs?REQUEST=GetCapabilities&SERVICE=WFS)

* [GeoServer Web Feature Service - Vlaamse Milieumaatschappij](https://geoserver.vmm.be/geoserver/HDGIS/wfs) - [GetCapabilities](https://geoserver.vmm.be/geoserver/HDGIS/wfs?REQUEST=GetCapabilities&SERVICE=WFS)

* [GeoServer Web Feature Service - geo.onroerenderfgoed.be](https://geo.onroerenderfgoed.be/geoserver/ows) - [GetCapabilities](https://geo.onroerenderfgoed.be/geoserver/ows?REQUEST=GetCapabilities&SERVICE=WFS)

* [GeoServer Web Feature Service - Gent](https://geo.gent.be/geoserver/SG-E-CultuurSportVrijetijd/wfs) - [GetCapabilities](https://geo.gent.be/geoserver/SG-E-CultuurSportVrijetijd/wfs?REQUEST=GetCapabilities&SERVICE=WFS)

* [GeoServer Web Feature Service - Gent](https://geo.gent.be/geoserver/SG-E-BasislagenLuchtfotos/wfs) - [GetCapabilities](https://geo.gent.be/geoserver/SG-E-BasislagenLuchtfotos/wfs?REQUEST=GetCapabilities&SERVICE=WFS)

* [GeoServer Web Feature Service - Gent](https://geo.gent.be/geoserver/SG-E-BouwenWonen/wfs) - [GetCapabilities](https://geo.gent.be/geoserver/SG-E-BouwenWonen/wfs?REQUEST=GetCapabilities&SERVICE=WFS)

* [geoserver-extern.ovam.be - geoserver-extern.ovam.be](https://geoserver-extern.ovam.be/geoserver/wfs) - [GetCapabilities](https://geoserver-extern.ovam.be/geoserver/wfs?REQUEST=GetCapabilities&SERVICE=WFS)

* [geoservice.waterinfo.be - Vlaamse Milieu Maatschappij](https://geoservice.waterinfo.be/ows) - [GetCapabilities](https://geoservice.waterinfo.be/ows?REQUEST=GetCapabilities&SERVICE=WFS)

* [Hydrografie - Fysieke wateren - agentschap Digitaal Vlaanderen](https://geo.api.vlaanderen.be/hy-p/wfs) - [GetCapabilities](https://geo.api.vlaanderen.be/hy-p/wfs?REQUEST=GetCapabilities&SERVICE=WFS)

* [Hydrografie - Netwerk - agentschap Digitaal Vlaanderen](https://geo.api.vlaanderen.be/hy-n/wfs) - [GetCapabilities](https://geo.api.vlaanderen.be/hy-n/wfs?REQUEST=GetCapabilities&SERVICE=WFS)

* [Indicatoren Vlaams Verkeerscentrum - indicatoren.verkeerscentrum.be](http://indicatoren.verkeerscentrum.be/geoserver/ows) - [GetCapabilities](http://indicatoren.verkeerscentrum.be/geoserver/ows?REQUEST=GetCapabilities&SERVICE=WFS)

* [Inspiratiekaart Renovatiebeleid - geo.production.ikrb.agifly.cloud](https://geo.production.ikrb.agifly.cloud/geoserver/ows) - [GetCapabilities](https://geo.production.ikrb.agifly.cloud/geoserver/ows?REQUEST=GetCapabilities&SERVICE=WFS)

* [INSPIRE - Annex I - aanduidingsobjecten- Agentschap Onroerend Erfgoed - Onroerend Erfgoed](https://haleconnect.com/ows/services/org.989.ebcff89d-1bed-48d3-b915-6158d060c53d_wfs) - [GetCapabilities](https://haleconnect.com/ows/services/org.989.ebcff89d-1bed-48d3-b915-6158d060c53d_wfs?REQUEST=GetCapabilities&SERVICE=WFS)

* [INSPIRE - Annex I - duinendecreet - Agentschap Natuur en Bos - Agentschap voor Natuur en Bos](https://haleconnect.com/ows/services/org.989.f7f7079d-9840-4608-bdd7-56174236bef6_wfs) - [GetCapabilities](https://haleconnect.com/ows/services/org.989.f7f7079d-9840-4608-bdd7-56174236bef6_wfs?REQUEST=GetCapabilities&SERVICE=WFS)

* [INSPIRE - Annex I - Natura2000 - Agentschap Natuur en Bos - Agentschap voor Natuur en Bos](https://haleconnect.com/ows/services/org.989.adbecc04-e6d0-48ad-ba11-4b3f7781f090_wfs) - [GetCapabilities](https://haleconnect.com/ows/services/org.989.adbecc04-e6d0-48ad-ba11-4b3f7781f090_wfs?REQUEST=GetCapabilities&SERVICE=WFS)

* [INSPIRE - Annex I - Ramsar-gebieden - Agentschap Natuur en Bos - Agentschap Natuur en Bos](https://haleconnect.com/ows/services/org.989.a32670c8-8dd5-43a8-af82-edba5392498a_wfs) - [GetCapabilities](https://haleconnect.com/ows/services/org.989.a32670c8-8dd5-43a8-af82-edba5392498a_wfs?REQUEST=GetCapabilities&SERVICE=WFS)

* [INSPIRE Downloaddienst Databank Ondergrond Vlaanderen - Vlaamse Overheid - Databank Ondergrond Vlaanderen (DOV)](https://www.dov.vlaanderen.be/geoserver/wfs) - [GetCapabilities](https://www.dov.vlaanderen.be/geoserver/wfs?REQUEST=GetCapabilities&SERVICE=WFS)

* [Landgebruik - Bestaand landgebruik - agentschap Digitaal Vlaanderen](https://geo.api.vlaanderen.be/elu/wfs) - [GetCapabilities](https://geo.api.vlaanderen.be/elu/wfs?REQUEST=GetCapabilities&SERVICE=WFS)

* [LiDAR pointcloud tiles and raster data: DTM, DSM - Digitaal Vlaanderen](https://remotesensing.vlaanderen.be/services/openlidar/wfs) - [GetCapabilities](https://remotesensing.vlaanderen.be/services/openlidar/wfs?REQUEST=GetCapabilities&SERVICE=WFS)

* [Mercator Publieke Download Service - Vlaamse Overheid - Beleidsdomein Omgeving - MercatorNet](https://www.mercator.vlaanderen.be/raadpleegdienstenmercatorpubliek/wfs) - [GetCapabilities](https://www.mercator.vlaanderen.be/raadpleegdienstenmercatorpubliek/wfs?REQUEST=GetCapabilities&SERVICE=WFS)

* [Mercator Publieke Download Service - Vlaamse Overheid - Beleidsdomein Omgeving - MercatorNet](https://mercator.vlaanderen.be/raadpleegdienstenmercatorpubliek/am/wfs) - [GetCapabilities](https://mercator.vlaanderen.be/raadpleegdienstenmercatorpubliek/am/wfs?REQUEST=GetCapabilities&SERVICE=WFS)

* [Mercator Publieke Download Service - Vlaamse Overheid - Beleidsdomein Omgeving - MercatorNet](https://mercator.vlaanderen.be/raadpleegdienstenmercatorpubliek/er/wfs) - [GetCapabilities](https://mercator.vlaanderen.be/raadpleegdienstenmercatorpubliek/er/wfs?REQUEST=GetCapabilities&SERVICE=WFS)

* [Mercator Publieke Download Service - Vlaamse Overheid - Beleidsdomein Omgeving - MercatorNet](https://mercator.vlaanderen.be/raadpleegdienstenmercatorpubliek/hb/wfs) - [GetCapabilities](https://mercator.vlaanderen.be/raadpleegdienstenmercatorpubliek/hb/wfs?REQUEST=GetCapabilities&SERVICE=WFS)

* [Mercator Publieke Download Service - Vlaamse Overheid - Beleidsdomein Omgeving - MercatorNet](https://mercator.vlaanderen.be/raadpleegdienstenmercatorpubliek/hh/wfs) - [GetCapabilities](https://mercator.vlaanderen.be/raadpleegdienstenmercatorpubliek/hh/wfs?REQUEST=GetCapabilities&SERVICE=WFS)

* [Mercator Publieke Download Service - Vlaamse Overheid - Beleidsdomein Omgeving - MercatorNet](https://mercator.vlaanderen.be/raadpleegdienstenmercatorpubliek/lu/wfs) - [GetCapabilities](https://mercator.vlaanderen.be/raadpleegdienstenmercatorpubliek/lu/wfs?REQUEST=GetCapabilities&SERVICE=WFS)

* [Mercator Publieke Download Service - Vlaamse Overheid - Beleidsdomein Omgeving - MercatorNet](https://mercator.vlaanderen.be/raadpleegdienstenmercatorpubliek/ni/wfs) - [GetCapabilities](https://mercator.vlaanderen.be/raadpleegdienstenmercatorpubliek/ni/wfs?REQUEST=GetCapabilities&SERVICE=WFS)

* [Mercator Publieke Download Service - Vlaamse Overheid - Beleidsdomein Omgeving - MercatorNet](https://mercator.vlaanderen.be/raadpleegdienstenmercatorpubliek/pf/wfs) - [GetCapabilities](https://mercator.vlaanderen.be/raadpleegdienstenmercatorpubliek/pf/wfs?REQUEST=GetCapabilities&SERVICE=WFS)

* [Mercator Publieke Download Service - Vlaamse Overheid - Beleidsdomein Omgeving - MercatorNet](https://mercator.vlaanderen.be/raadpleegdienstenmercatorpubliek/ps/wfs) - [GetCapabilities](https://mercator.vlaanderen.be/raadpleegdienstenmercatorpubliek/ps/wfs?REQUEST=GetCapabilities&SERVICE=WFS)

* [Mercator Publieke Download Service - Vlaamse Overheid - Beleidsdomein Omgeving - MercatorNet](https://mercator.vlaanderen.be/raadpleegdienstenmercatorpubliek/so/wfs) - [GetCapabilities](https://mercator.vlaanderen.be/raadpleegdienstenmercatorpubliek/so/wfs?REQUEST=GetCapabilities&SERVICE=WFS)

* [Mercator Publieke Download Service - Vlaamse Overheid - Beleidsdomein Omgeving - MercatorNet](https://mercator.vlaanderen.be/raadpleegdienstenmercatorpubliek/us/wfs) - [GetCapabilities](https://mercator.vlaanderen.be/raadpleegdienstenmercatorpubliek/us/wfs?REQUEST=GetCapabilities&SERVICE=WFS)

* [Milieubewakingsvoorzieningen - agentschap Digitaal Vlaanderen](https://geo.api.vlaanderen.be/ef/wfs) - [GetCapabilities](https://geo.api.vlaanderen.be/ef/wfs?REQUEST=GetCapabilities&SERVICE=WFS)

* [Nutsdiensten en overheidsdiensten - Inrichtingen milieubeheer - agentschap Digitaal Vlaanderen](https://geo.api.vlaanderen.be/us-emf/wfs) - [GetCapabilities](https://geo.api.vlaanderen.be/us-emf/wfs?REQUEST=GetCapabilities&SERVICE=WFS)

* [Nutsdiensten en overheidsdiensten - Nutsvoorzieningennet - agentschap Digitaal Vlaanderen](https://geo.api.vlaanderen.be/us-net-common/wfs) - [GetCapabilities](https://geo.api.vlaanderen.be/us-net-common/wfs?REQUEST=GetCapabilities&SERVICE=WFS)

* [Nutsdiensten en overheidsdiensten - Rioleringsnet - agentschap Digitaal Vlaanderen](https://geo.api.vlaanderen.be/us-net-sw/wfs) - [GetCapabilities](https://geo.api.vlaanderen.be/us-net-sw/wfs?REQUEST=GetCapabilities&SERVICE=WFS)

* [remote-sensing.be - Programma Earth Observation Data Science (EODaS), Agentschap Informatie Vlaanderen](https://remote-sensing.be/geoserver/VEA/wfs) - [GetCapabilities](https://remote-sensing.be/geoserver/VEA/wfs?REQUEST=GetCapabilities&SERVICE=WFS)

* [Vervoersnetwerken weg - agentschap Digitaal Vlaanderen](https://geo.api.vlaanderen.be/tn-ro/wfs) - [GetCapabilities](https://geo.api.vlaanderen.be/tn-ro/wfs?REQUEST=GetCapabilities&SERVICE=WFS)

* [WFS - gis.provincieantwerpen.be](https://gis.provincieantwerpen.be/arcgis/services/Publiek/AtlasBuurtwegen/MapServer/WFSServer) - [GetCapabilities](https://gis.provincieantwerpen.be/arcgis/services/Publiek/AtlasBuurtwegen/MapServer/WFSServer?REQUEST=GetCapabilities&SERVICE=WFS)

* [WFS - gis.provincieantwerpen.be](https://gis.provincieantwerpen.be/arcgis/services/Publiek/PRUP/MapServer/WFSServer) - [GetCapabilities](https://gis.provincieantwerpen.be/arcgis/services/Publiek/PRUP/MapServer/WFSServer?REQUEST=GetCapabilities&SERVICE=WFS)

* [WFS - geoservices.vlaamsbrabant.be](https://geoservices.vlaamsbrabant.be/RUPs/MapServer/WFSServer) - [GetCapabilities](https://geoservices.vlaamsbrabant.be/RUPs/MapServer/WFSServer?REQUEST=GetCapabilities&SERVICE=WFS)

* [WFS - geoservices.vlaamsbrabant.be](https://geoservices.vlaamsbrabant.be/AtlasWaterlopen/MapServer/WFSServer) - [GetCapabilities](https://geoservices.vlaamsbrabant.be/AtlasWaterlopen/MapServer/WFSServer?REQUEST=GetCapabilities&SERVICE=WFS)

* [WFS - geoservices.vlaamsbrabant.be](https://geoservices.vlaamsbrabant.be/AtlasBuurtwegen_wijzigingen/MapServer/WFSServer) - [GetCapabilities](https://geoservices.vlaamsbrabant.be/AtlasBuurtwegen_wijzigingen/MapServer/WFSServer?REQUEST=GetCapabilities&SERVICE=WFS)

* [WFS - gis.provincieantwerpen.be](https://gis.provincieantwerpen.be/arcgis/services/Publiek/AtlasWaterlopen/MapServer/WFSServer) - [GetCapabilities](https://gis.provincieantwerpen.be/arcgis/services/Publiek/AtlasWaterlopen/MapServer/WFSServer?REQUEST=GetCapabilities&SERVICE=WFS)

* [WFS - gis.provincieantwerpen.be](https://gis.provincieantwerpen.be/arcgis/services/Publiek/Ms_Fiets_BFF/MapServer/WFSServer) - [GetCapabilities](https://gis.provincieantwerpen.be/arcgis/services/Publiek/Ms_Fiets_BFF/MapServer/WFSServer?REQUEST=GetCapabilities&SERVICE=WFS)

* [WFS - inspirepub.waterinfo.be](https://inspirepub.waterinfo.be/arcgis/services/waterinfo_WFS/MapServer/WFSServer) - [GetCapabilities](https://inspirepub.waterinfo.be/arcgis/services/waterinfo_WFS/MapServer/WFSServer?REQUEST=GetCapabilities&SERVICE=WFS)

* [WFS - geoservices.vlaamsbrabant.be](https://geoservices.vlaamsbrabant.be/TrageWegen/MapServer/WFSServer) - [GetCapabilities](https://geoservices.vlaamsbrabant.be/TrageWegen/MapServer/WFSServer?REQUEST=GetCapabilities&SERVICE=WFS)

* [WFS - geoservices.vlaamsbrabant.be](https://geoservices.vlaamsbrabant.be/BFF/MapServer/WFSServer) - [GetCapabilities](https://geoservices.vlaamsbrabant.be/BFF/MapServer/WFSServer?REQUEST=GetCapabilities&SERVICE=WFS)

* [WFS - inspirepub.waterinfo.be](https://inspirepub.waterinfo.be/arcgis/services/ANB/zones/MapServer/WFSServer) - [GetCapabilities](https://inspirepub.waterinfo.be/arcgis/services/ANB/zones/MapServer/WFSServer?REQUEST=GetCapabilities&SERVICE=WFS)

* [WFS - gis.provincieantwerpen.be](https://gis.provincieantwerpen.be/arcgis/services/Publiek/AtlasWaterlopen_WFS/MapServer/WFSServer) - [GetCapabilities](https://gis.provincieantwerpen.be/arcgis/services/Publiek/AtlasWaterlopen_WFS/MapServer/WFSServer?REQUEST=GetCapabilities&SERVICE=WFS)

* [WFS - inspirepub.waterinfo.be](https://inspirepub.waterinfo.be/arcgis/services/meetpunten_MOW/MapServer/WFSServer) - [GetCapabilities](https://inspirepub.waterinfo.be/arcgis/services/meetpunten_MOW/MapServer/WFSServer?REQUEST=GetCapabilities&SERVICE=WFS)

* [WFS - inspirepub.waterinfo.be](https://inspirepub.waterinfo.be/arcgis/services/meetpunten/MapServer/WFSServer) - [GetCapabilities](https://inspirepub.waterinfo.be/arcgis/services/meetpunten/MapServer/WFSServer?REQUEST=GetCapabilities&SERVICE=WFS)

* [WFS - geoservices.vlaamsbrabant.be](https://geoservices.vlaamsbrabant.be/RecreatieveNetwerken/MapServer/WFSServer) - [GetCapabilities](https://geoservices.vlaamsbrabant.be/RecreatieveNetwerken/MapServer/WFSServer?REQUEST=GetCapabilities&SERVICE=WFS)

* [WFS - Provincie Limburg](https://geo.limburg.be/arcgis/services/ABW/MapServer/WFSServer) - [GetCapabilities](https://geo.limburg.be/arcgis/services/ABW/MapServer/WFSServer?REQUEST=GetCapabilities&SERVICE=WFS)

* [WFS - geo.limburg.be](https://geo.limburg.be/arcgis/services/Mobiliteit_FSW_be/MapServer/WFSServer) - [GetCapabilities](https://geo.limburg.be/arcgis/services/Mobiliteit_FSW_be/MapServer/WFSServer?REQUEST=GetCapabilities&SERVICE=WFS)

* [WFS - Provincie Limburg](https://geo.limburg.be/arcgis/services/Mobiliteit_fiets/MapServer/WFSServer) - [GetCapabilities](https://geo.limburg.be/arcgis/services/Mobiliteit_fiets/MapServer/WFSServer?REQUEST=GetCapabilities&SERVICE=WFS)

* [WFS - Provincie Limburg](https://geo.limburg.be/arcgis/services/Mobiliteit_fietssnelwegen_be/MapServer/WFSServer) - [GetCapabilities](https://geo.limburg.be/arcgis/services/Mobiliteit_fietssnelwegen_be/MapServer/WFSServer?REQUEST=GetCapabilities&SERVICE=WFS)

* [WFS - geo.limburg.be](https://geo.limburg.be/arcgis/services/VHA_werkversie/MapServer/WFSServer) - [GetCapabilities](https://geo.limburg.be/arcgis/services/VHA_werkversie/MapServer/WFSServer?REQUEST=GetCapabilities&SERVICE=WFS)

* [WFS - GISWest](https://www.geoloket.be/gwserver/services/Mobiliteit/AGS_Atlas_bw_wijziging/MapServer/WFSServer) - [GetCapabilities](https://www.geoloket.be/gwserver/services/Mobiliteit/AGS_Atlas_bw_wijziging/MapServer/WFSServer?REQUEST=GetCapabilities&SERVICE=WFS)

* [WFS - GISWest](https://www.geoloket.be/gwserver/services/Mobiliteit/AGS_fietsroutenetwerk/MapServer/WFSServer) - [GetCapabilities](https://www.geoloket.be/gwserver/services/Mobiliteit/AGS_fietsroutenetwerk/MapServer/WFSServer?REQUEST=GetCapabilities&SERVICE=WFS)

* [WFS - vha.waterinfo.be](https://vha.waterinfo.be/arcgis/services/advieskaart_watertoets_WFS/MapServer/WFSServer) - [GetCapabilities](https://vha.waterinfo.be/arcgis/services/advieskaart_watertoets_WFS/MapServer/WFSServer?REQUEST=GetCapabilities&SERVICE=WFS)

* [WFS - inspirepub.waterinfo.be](https://inspirepub.waterinfo.be/arcgis/services/afstroomgebieden_watertoets/MapServer/WFSServer) - [GetCapabilities](https://inspirepub.waterinfo.be/arcgis/services/afstroomgebieden_watertoets/MapServer/WFSServer?REQUEST=GetCapabilities&SERVICE=WFS)

* [WFS - vha.waterinfo.be](https://vha.waterinfo.be/arcgis/services/waterlopen/MapServer/WFSServer) - [GetCapabilities](https://vha.waterinfo.be/arcgis/services/waterlopen/MapServer/WFSServer?REQUEST=GetCapabilities&SERVICE=WFS)

* [WFS - dservices-eu1.arcgis.com](https://dservices-eu1.arcgis.com/0BOxFMmSWQhAlNXX/arcgis/services/INSPIREWaterway/WFSServer) - [GetCapabilities](https://dservices-eu1.arcgis.com/0BOxFMmSWQhAlNXX/arcgis/services/INSPIREWaterway/WFSServer?REQUEST=GetCapabilities&SERVICE=WFS)

* [WFS - gis.provincieantwerpen.be](https://gis.provincieantwerpen.be/arcgis/services/FSW/Fietssnelwegen/MapServer/WFSServer) - [GetCapabilities](https://gis.provincieantwerpen.be/arcgis/services/FSW/Fietssnelwegen/MapServer/WFSServer?REQUEST=GetCapabilities&SERVICE=WFS)

* [WFS - gis.provincieantwerpen.be](https://gis.provincieantwerpen.be/arcgis/services/Publiek/Ms_Fietsdata/MapServer/WFSServer) - [GetCapabilities](https://gis.provincieantwerpen.be/arcgis/services/Publiek/Ms_Fietsdata/MapServer/WFSServer?REQUEST=GetCapabilities&SERVICE=WFS)

* [WFS - inspirepub.waterinfo.be](https://inspirepub.waterinfo.be/arcgis/services/VHA_waterlopen/MapServer/WFSServer) - [GetCapabilities](https://inspirepub.waterinfo.be/arcgis/services/VHA_waterlopen/MapServer/WFSServer?REQUEST=GetCapabilities&SERVICE=WFS)

* [WFS - inspirepub.waterinfo.be](https://inspirepub.waterinfo.be/arcgis/services/captatie/captatieverboden/MapServer/WFSServer) - [GetCapabilities](https://inspirepub.waterinfo.be/arcgis/services/captatie/captatieverboden/MapServer/WFSServer?REQUEST=GetCapabilities&SERVICE=WFS)

* [WFS - geoservices.vlaamsbrabant.be](https://geoservices.vlaamsbrabant.be/FSW/MapServer/WFSServer) - [GetCapabilities](https://geoservices.vlaamsbrabant.be/FSW/MapServer/WFSServer?REQUEST=GetCapabilities&SERVICE=WFS)

* [WFS - gis.provincieantwerpen.be](https://gis.provincieantwerpen.be/arcgis/services/Publiek/BFF_Wegenregister/MapServer/WFSServer) - [GetCapabilities](https://gis.provincieantwerpen.be/arcgis/services/Publiek/BFF_Wegenregister/MapServer/WFSServer?REQUEST=GetCapabilities&SERVICE=WFS)

* [WFS - vha.waterinfo.be](https://vha.waterinfo.be/arcgis/services/Kunstwerken/MapServer/WFSServer) - [GetCapabilities](https://vha.waterinfo.be/arcgis/services/Kunstwerken/MapServer/WFSServer?REQUEST=GetCapabilities&SERVICE=WFS)

* [WFS - vha.waterinfo.be](https://vha.waterinfo.be/arcgis/services/extralagen/MapServer/WFSServer) - [GetCapabilities](https://vha.waterinfo.be/arcgis/services/extralagen/MapServer/WFSServer?REQUEST=GetCapabilities&SERVICE=WFS)

* [WFS - vha.waterinfo.be](https://vha.waterinfo.be/arcgis/services/meldingen/MapServer/WFSServer) - [GetCapabilities](https://vha.waterinfo.be/arcgis/services/meldingen/MapServer/WFSServer?REQUEST=GetCapabilities&SERVICE=WFS)

* [WFS - vha.waterinfo.be](https://vha.waterinfo.be/arcgis/services/voortoets/MapServer/WFSServer) - [GetCapabilities](https://vha.waterinfo.be/arcgis/services/voortoets/MapServer/WFSServer?REQUEST=GetCapabilities&SERVICE=WFS)

* [WFS Adressenregister - agentschap Digitaal Vlaanderen](https://geo.api.vlaanderen.be/Adressenregister/wfs) - [GetCapabilities](https://geo.api.vlaanderen.be/Adressenregister/wfs?REQUEST=GetCapabilities&SERVICE=WFS)

* [WFS Agentschap Wegen en Verkeer - Agentschap Wegen en Verkeer](https://opendata.apps.mow.vlaanderen.be/opendata-geoserver/awv/wfs) - [GetCapabilities](https://opendata.apps.mow.vlaanderen.be/opendata-geoserver/awv/wfs?REQUEST=GetCapabilities&SERVICE=WFS)

* [WFS Agglomeraties Richtlijn Stedelijk afvalwater - agentschap Digitaal Vlaanderen](https://geo.api.vlaanderen.be/Uwwtd-Agglomerations/wfs) - [GetCapabilities](https://geo.api.vlaanderen.be/Uwwtd-Agglomerations/wfs?REQUEST=GetCapabilities&SERVICE=WFS)

* [WFS Bebossing op de Ferrariskaarten - agentschap Digitaal Vlaanderen](https://geo.api.vlaanderen.be/BosFerraris/wfs) - [GetCapabilities](https://geo.api.vlaanderen.be/BosFerraris/wfs?REQUEST=GetCapabilities&SERVICE=WFS)

* [WFS Bebossing op de topografische kaarten - agentschap Digitaal Vlaanderen](https://geo.api.vlaanderen.be/BosTopokaart/wfs) - [GetCapabilities](https://geo.api.vlaanderen.be/BosTopokaart/wfs?REQUEST=GetCapabilities&SERVICE=WFS)

* [WFS Bebossing op de Vandermaelenkaarten - agentschap Digitaal Vlaanderen](https://geo.api.vlaanderen.be/BosVandermaelen/wfs) - [GetCapabilities](https://geo.api.vlaanderen.be/BosVandermaelen/wfs?REQUEST=GetCapabilities&SERVICE=WFS)

* [WFS Bedrijventerreinen - agentschap Digitaal Vlaanderen](https://geo.api.vlaanderen.be/Bedrijventerreinen/wfs) - [GetCapabilities](https://geo.api.vlaanderen.be/Bedrijventerreinen/wfs?REQUEST=GetCapabilities&SERVICE=WFS)

* [WFS Beschermde gebieden ifv winning drinkwater uit oppervlakte- en grondwater Kaderrichtlijn water - agentschap Digitaal Vlaanderen](https://geo.api.vlaanderen.be/WFD-ProtectedArea/wfs) - [GetCapabilities](https://geo.api.vlaanderen.be/WFD-ProtectedArea/wfs?REQUEST=GetCapabilities&SERVICE=WFS)

* [WFS Biologische Waarderingskaart en Natura 2000 Habitatkaart - agentschap Digitaal Vlaanderen](https://geo.api.vlaanderen.be/BWK/wfs) - [GetCapabilities](https://geo.api.vlaanderen.be/BWK/wfs?REQUEST=GetCapabilities&SERVICE=WFS)

* [WFS Bosleeftijd - agentschap Digitaal Vlaanderen](https://geo.api.vlaanderen.be/Bosleeftijd/wfs) - [GetCapabilities](https://geo.api.vlaanderen.be/Bosleeftijd/wfs?REQUEST=GetCapabilities&SERVICE=WFS)

* [WFS Bosreferentielaag 2000 - agentschap Digitaal Vlaanderen](https://geo.api.vlaanderen.be/Bosref/wfs) - [GetCapabilities](https://geo.api.vlaanderen.be/Bosref/wfs?REQUEST=GetCapabilities&SERVICE=WFS)

* [WFS Brownfieldconvenanten - agentschap Digitaal Vlaanderen](https://geo.api.vlaanderen.be/Brownfieldconvenanten/wfs) - [GetCapabilities](https://geo.api.vlaanderen.be/Brownfieldconvenanten/wfs?REQUEST=GetCapabilities&SERVICE=WFS)

* [WFS EU Register van industriële sites - agentschap Digitaal Vlaanderen](https://geo.api.vlaanderen.be/EUReg-EPRTR/wfs) - [GetCapabilities](https://geo.api.vlaanderen.be/EUReg-EPRTR/wfs?REQUEST=GetCapabilities&SERVICE=WFS)

* [WFS Gebieden met recht van voorkoop - agentschap Digitaal Vlaanderen](https://geo.api.vlaanderen.be/RVV/wfs) - [GetCapabilities](https://geo.api.vlaanderen.be/RVV/wfs?REQUEST=GetCapabilities&SERVICE=WFS)

* [WFS Gebiedstypes nitraat mestdecreet - agentschap Digitaal Vlaanderen](https://geo.api.vlaanderen.be/Gebnit/wfs) - [GetCapabilities](https://geo.api.vlaanderen.be/Gebnit/wfs?REQUEST=GetCapabilities&SERVICE=WFS)

* [WFS Gebouwen Vlaamse en Lokale Overheden - agentschap Digitaal Vlaanderen](https://geo.api.vlaanderen.be/GebouwenVLO/wfs) - [GetCapabilities](https://geo.api.vlaanderen.be/GebouwenVLO/wfs?REQUEST=GetCapabilities&SERVICE=WFS)

* [WFS Gebouwenregister - agentschap Digitaal Vlaanderen](https://geo.api.vlaanderen.be/Gebouwenregister/wfs) - [GetCapabilities](https://geo.api.vlaanderen.be/Gebouwenregister/wfs?REQUEST=GetCapabilities&SERVICE=WFS)

* [WFS GIPOD - agentschap Digitaal Vlaanderen](https://geo.api.vlaanderen.be/GIPOD/wfs) - [GetCapabilities](https://geo.api.vlaanderen.be/GIPOD/wfs?REQUEST=GetCapabilities&SERVICE=WFS)

* [WFS GRB - agentschap Digitaal Vlaanderen](https://geo.api.vlaanderen.be/GRB/wfs) - [GetCapabilities](https://geo.api.vlaanderen.be/GRB/wfs?REQUEST=GetCapabilities&SERVICE=WFS)

* [WFS GRB - Administratieve percelen fiscaal - agentschap Digitaal Vlaanderen](https://geo.api.vlaanderen.be/Adpf/wfs) - [GetCapabilities](https://geo.api.vlaanderen.be/Adpf/wfs?REQUEST=GetCapabilities&SERVICE=WFS)

* [WFS Grenzen van Polders - agentschap Digitaal Vlaanderen](https://geo.api.vlaanderen.be/Polders/wfs) - [GetCapabilities](https://geo.api.vlaanderen.be/Polders/wfs?REQUEST=GetCapabilities&SERVICE=WFS)

* [WFS Grenzen van wateringen - agentschap Digitaal Vlaanderen](https://geo.api.vlaanderen.be/Wateringen/wfs) - [GetCapabilities](https://geo.api.vlaanderen.be/Wateringen/wfs?REQUEST=GetCapabilities&SERVICE=WFS)

* [WFS Haltes De Lijn - agentschap Digitaal Vlaanderen](https://geo.api.vlaanderen.be/Haltes/wfs) - [GetCapabilities](https://geo.api.vlaanderen.be/Haltes/wfs?REQUEST=GetCapabilities&SERVICE=WFS)

* [WFS Historisch landgebruik - agentschap Digitaal Vlaanderen](https://geo.api.vlaanderen.be/HistLandgebruik/wfs) - [GetCapabilities](https://geo.api.vlaanderen.be/HistLandgebruik/wfs?REQUEST=GetCapabilities&SERVICE=WFS)

* [WFS INSPIRE geharmoniseerd - Databank Ondergrond Vlaanderen - Vlaamse Overheid - Databank Ondergrond Vlaanderen (DOV)](https://www.dov.vlaanderen.be/geoserver-inspire/landslide/wfs) - [GetCapabilities](https://www.dov.vlaanderen.be/geoserver-inspire/landslide/wfs?REQUEST=GetCapabilities&SERVICE=WFS)

* [WFS INSPIRE geharmoniseerd - Databank Ondergrond Vlaanderen - Vlaamse Overheid - Databank Ondergrond Vlaanderen (DOV)](https://www.dov.vlaanderen.be/geoserver-inspire/organiccarboncontent/wfs) - [GetCapabilities](https://www.dov.vlaanderen.be/geoserver-inspire/organiccarboncontent/wfs?REQUEST=GetCapabilities&SERVICE=WFS)

* [WFS INSPIRE geharmoniseerd - Databank Ondergrond Vlaanderen - Vlaamse Overheid - Databank Ondergrond Vlaanderen (DOV)](https://www.dov.vlaanderen.be/geoserver-inspire/flightline/wfs) - [GetCapabilities](https://www.dov.vlaanderen.be/geoserver-inspire/flightline/wfs?REQUEST=GetCapabilities&SERVICE=WFS)

* [WFS INSPIRE geharmoniseerd - Databank Ondergrond Vlaanderen - Vlaamse Overheid - Databank Ondergrond Vlaanderen (DOV)](https://www.dov.vlaanderen.be/geoserver-inspire/seismicline/wfs) - [GetCapabilities](https://www.dov.vlaanderen.be/geoserver-inspire/seismicline/wfs?REQUEST=GetCapabilities&SERVICE=WFS)

* [WFS INSPIRE geharmoniseerd - Databank Ondergrond Vlaanderen - Vlaamse Overheid - Databank Ondergrond Vlaanderen (DOV)](https://www.dov.vlaanderen.be/geoserver-inspire/aquifer/wfs) - [GetCapabilities](https://www.dov.vlaanderen.be/geoserver-inspire/aquifer/wfs?REQUEST=GetCapabilities&SERVICE=WFS)

* [WFS INSPIRE geharmoniseerd - Databank Ondergrond Vlaanderen - Vlaamse Overheid - Databank Ondergrond Vlaanderen (DOV)](https://www.dov.vlaanderen.be/geoserver-inspire/environmentalmonitoringfacility/wfs) - [GetCapabilities](https://www.dov.vlaanderen.be/geoserver-inspire/environmentalmonitoringfacility/wfs?REQUEST=GetCapabilities&SERVICE=WFS)

* [WFS INSPIRE geharmoniseerd - Databank Ondergrond Vlaanderen - Vlaamse Overheid - Databank Ondergrond Vlaanderen (DOV)](https://www.dov.vlaanderen.be/geoserver-inspire/mineraloccurrence/wfs) - [GetCapabilities](https://www.dov.vlaanderen.be/geoserver-inspire/mineraloccurrence/wfs?REQUEST=GetCapabilities&SERVICE=WFS)

* [WFS INSPIRE geharmoniseerd - Databank Ondergrond Vlaanderen - Vlaamse Overheid - Databank Ondergrond Vlaanderen (DOV)](https://www.dov.vlaanderen.be/geoserver-inspire/mine/wfs) - [GetCapabilities](https://www.dov.vlaanderen.be/geoserver-inspire/mine/wfs?REQUEST=GetCapabilities&SERVICE=WFS)

* [WFS INSPIRE geharmoniseerd - Databank Ondergrond Vlaanderen - Vlaamse Overheid - Databank Ondergrond Vlaanderen (DOV)](https://www.dov.vlaanderen.be/geoserver-inspire/phvalue/wfs) - [GetCapabilities](https://www.dov.vlaanderen.be/geoserver-inspire/phvalue/wfs?REQUEST=GetCapabilities&SERVICE=WFS)

* [WFS INSPIRE geharmoniseerd - Databank Ondergrond Vlaanderen - Vlaamse Overheid - Databank Ondergrond Vlaanderen (DOV)](https://www.dov.vlaanderen.be/geoserver-inspire/soilsite/wfs) - [GetCapabilities](https://www.dov.vlaanderen.be/geoserver-inspire/soilsite/wfs?REQUEST=GetCapabilities&SERVICE=WFS)

* [WFS INSPIRE geharmoniseerd - Databank Ondergrond Vlaanderen - Vlaamse Overheid - Databank Ondergrond Vlaanderen (DOV)](https://www.dov.vlaanderen.be/geoserver-inspire/gravitystation/wfs) - [GetCapabilities](https://www.dov.vlaanderen.be/geoserver-inspire/gravitystation/wfs?REQUEST=GetCapabilities&SERVICE=WFS)

* [WFS INSPIRE geharmoniseerd - Databank Ondergrond Vlaanderen - Vlaamse Overheid - Databank Ondergrond Vlaanderen (DOV)](https://www.dov.vlaanderen.be/geoserver-inspire/aquitard/wfs) - [GetCapabilities](https://www.dov.vlaanderen.be/geoserver-inspire/aquitard/wfs?REQUEST=GetCapabilities&SERVICE=WFS)

* [WFS INSPIRE geharmoniseerd - Databank Ondergrond Vlaanderen - Vlaamse Overheid - Databank Ondergrond Vlaanderen (DOV)](https://www.dov.vlaanderen.be/geoserver-inspire/boreholelogging/wfs) - [GetCapabilities](https://www.dov.vlaanderen.be/geoserver-inspire/boreholelogging/wfs?REQUEST=GetCapabilities&SERVICE=WFS)

* [WFS INSPIRE geharmoniseerd - Databank Ondergrond Vlaanderen - Vlaamse Overheid - Databank Ondergrond Vlaanderen (DOV)](https://www.dov.vlaanderen.be/geoserver-inspire/waterbodyforwfd/wfs) - [GetCapabilities](https://www.dov.vlaanderen.be/geoserver-inspire/waterbodyforwfd/wfs?REQUEST=GetCapabilities&SERVICE=WFS)

* [WFS INSPIRE geharmoniseerd - Databank Ondergrond Vlaanderen - Vlaamse Overheid - Databank Ondergrond Vlaanderen (DOV)](https://www.dov.vlaanderen.be/geoserver-inspire/observedsoilprofile/wfs) - [GetCapabilities](https://www.dov.vlaanderen.be/geoserver-inspire/observedsoilprofile/wfs?REQUEST=GetCapabilities&SERVICE=WFS)

* [WFS INSPIRE geharmoniseerd - Databank Ondergrond Vlaanderen - Vlaamse Overheid - Databank Ondergrond Vlaanderen (DOV)](https://www.dov.vlaanderen.be/geoserver-inspire/activewell/wfs) - [GetCapabilities](https://www.dov.vlaanderen.be/geoserver-inspire/activewell/wfs?REQUEST=GetCapabilities&SERVICE=WFS)

* [WFS INSPIRE geharmoniseerd - Databank Ondergrond Vlaanderen - Vlaamse Overheid - Databank Ondergrond Vlaanderen (DOV)](https://www.dov.vlaanderen.be/geoserver-inspire/conepenetrationtest/wfs) - [GetCapabilities](https://www.dov.vlaanderen.be/geoserver-inspire/conepenetrationtest/wfs?REQUEST=GetCapabilities&SERVICE=WFS)

* [WFS INSPIRE geharmoniseerd - Databank Ondergrond Vlaanderen - Vlaamse Overheid - Databank Ondergrond Vlaanderen (DOV)](https://www.dov.vlaanderen.be/geoserver-inspire/aquifersystem/wfs) - [GetCapabilities](https://www.dov.vlaanderen.be/geoserver-inspire/aquifersystem/wfs?REQUEST=GetCapabilities&SERVICE=WFS)

* [WFS INSPIRE geharmoniseerd - Databank Ondergrond Vlaanderen - Vlaamse Overheid - Databank Ondergrond Vlaanderen (DOV)](https://www.dov.vlaanderen.be/geoserver-inspire/borehole/wfs) - [GetCapabilities](https://www.dov.vlaanderen.be/geoserver-inspire/borehole/wfs?REQUEST=GetCapabilities&SERVICE=WFS)

* [WFS INSPIRE geharmoniseerd - Databank Ondergrond Vlaanderen - Vlaamse Overheid - Databank Ondergrond Vlaanderen (DOV)](https://www.dov.vlaanderen.be/geoserver-inspire/waterbodyforwfdhorizon/wfs) - [GetCapabilities](https://www.dov.vlaanderen.be/geoserver-inspire/waterbodyforwfdhorizon/wfs?REQUEST=GetCapabilities&SERVICE=WFS)

* [WFS INSPIRE geharmoniseerd - Databank Ondergrond Vlaanderen - Vlaamse Overheid - Databank Ondergrond Vlaanderen (DOV)](https://www.dov.vlaanderen.be/geoserver-inspire/geologicfault/wfs) - [GetCapabilities](https://www.dov.vlaanderen.be/geoserver-inspire/geologicfault/wfs?REQUEST=GetCapabilities&SERVICE=WFS)

* [WFS INSPIRE geharmoniseerd - Databank Ondergrond Vlaanderen - Vlaamse Overheid - Databank Ondergrond Vlaanderen (DOV)](https://www.dov.vlaanderen.be/geoserver-inspire/biologicalparameter/wfs) - [GetCapabilities](https://www.dov.vlaanderen.be/geoserver-inspire/biologicalparameter/wfs?REQUEST=GetCapabilities&SERVICE=WFS)

* [WFS INSPIRE geharmoniseerd - Databank Ondergrond Vlaanderen - Vlaamse Overheid - Databank Ondergrond Vlaanderen (DOV)](https://www.dov.vlaanderen.be/geoserver-inspire/physicalparameter/wfs) - [GetCapabilities](https://www.dov.vlaanderen.be/geoserver-inspire/physicalparameter/wfs?REQUEST=GetCapabilities&SERVICE=WFS)

* [WFS INSPIRE geharmoniseerd - Databank Ondergrond Vlaanderen - Vlaamse Overheid - Databank Ondergrond Vlaanderen (DOV)](https://www.dov.vlaanderen.be/geoserver-inspire/naturalgeomorphologicfeature/wfs) - [GetCapabilities](https://www.dov.vlaanderen.be/geoserver-inspire/naturalgeomorphologicfeature/wfs?REQUEST=GetCapabilities&SERVICE=WFS)

* [WFS INSPIRE geharmoniseerd - Databank Ondergrond Vlaanderen - Vlaamse Overheid - Databank Ondergrond Vlaanderen (DOV)](https://www.dov.vlaanderen.be/geoserver-inspire/chemicalparameter/wfs) - [GetCapabilities](https://www.dov.vlaanderen.be/geoserver-inspire/chemicalparameter/wfs?REQUEST=GetCapabilities&SERVICE=WFS)

* [WFS INSPIRE geharmoniseerd - Databank Ondergrond Vlaanderen - Vlaamse Overheid - Databank Ondergrond Vlaanderen (DOV)](https://www.dov.vlaanderen.be/geoserver-inspire/geologicunit/wfs) - [GetCapabilities](https://www.dov.vlaanderen.be/geoserver-inspire/geologicunit/wfs?REQUEST=GetCapabilities&SERVICE=WFS)

* [WFS INSPIRE geharmoniseerd - Databank Ondergrond Vlaanderen - Vlaamse Overheid - Databank Ondergrond Vlaanderen (DOV)](https://www.dov.vlaanderen.be/geoserver-inspire/prospectingandminingpermitarea/wfs) - [GetCapabilities](https://www.dov.vlaanderen.be/geoserver-inspire/prospectingandminingpermitarea/wfs?REQUEST=GetCapabilities&SERVICE=WFS)

* [WFS INSPIRE geharmoniseerd - Databank Ondergrond Vlaanderen - Vlaamse Overheid - Databank Ondergrond Vlaanderen (DOV)](https://www.dov.vlaanderen.be/geoserver-inspire/drinkingwaterprotectionarea/wfs) - [GetCapabilities](https://www.dov.vlaanderen.be/geoserver-inspire/drinkingwaterprotectionarea/wfs?REQUEST=GetCapabilities&SERVICE=WFS)

* [WFS INSPIRE geharmoniseerd - Databank Ondergrond Vlaanderen - Vlaamse Overheid - Databank Ondergrond Vlaanderen (DOV)](https://www.dov.vlaanderen.be/geoserver-inspire/soilbody/wfs) - [GetCapabilities](https://www.dov.vlaanderen.be/geoserver-inspire/soilbody/wfs?REQUEST=GetCapabilities&SERVICE=WFS)

* [WFS INSPIRE geharmoniseerd - Databank Ondergrond Vlaanderen - Vlaamse Overheid - Databank Ondergrond Vlaanderen (DOV)](https://www.dov.vlaanderen.be/geoserver-inspire/riskzone/wfs) - [GetCapabilities](https://www.dov.vlaanderen.be/geoserver-inspire/riskzone/wfs?REQUEST=GetCapabilities&SERVICE=WFS)

* [WFS INSPIRE geharmoniseerd - Databank Ondergrond Vlaanderen - Vlaamse Overheid - Databank Ondergrond Vlaanderen (DOV)](https://www.dov.vlaanderen.be/geoserver-inspire/exposedelement/wfs) - [GetCapabilities](https://www.dov.vlaanderen.be/geoserver-inspire/exposedelement/wfs?REQUEST=GetCapabilities&SERVICE=WFS)

* [WFS Interessante Plaatsen (POI) - agentschap Digitaal Vlaanderen](https://geo.api.vlaanderen.be/POI/wfs) - [GetCapabilities](https://geo.api.vlaanderen.be/POI/wfs?REQUEST=GetCapabilities&SERVICE=WFS)

* [WFS Jachtterreinen - agentschap Digitaal Vlaanderen](https://geo.api.vlaanderen.be/Jacht/wfs) - [GetCapabilities](https://geo.api.vlaanderen.be/Jacht/wfs?REQUEST=GetCapabilities&SERVICE=WFS)

* [WFS kwetsbare gebieden Nitraatrichtlijn - agentschap Digitaal Vlaanderen](https://geo.api.vlaanderen.be/NID-NVZ/wfs) - [GetCapabilities](https://geo.api.vlaanderen.be/NID-NVZ/wfs?REQUEST=GetCapabilities&SERVICE=WFS)

* [WFS Kwetsbare gebieden Richtlijn Stedelijk afvalwater - agentschap Digitaal Vlaanderen](https://geo.api.vlaanderen.be/Uwwtd-SensitiveAreas/wfs) - [GetCapabilities](https://geo.api.vlaanderen.be/Uwwtd-SensitiveAreas/wfs?REQUEST=GetCapabilities&SERVICE=WFS)

* [WFS Landbouwgebruikspercelen - agentschap Digitaal Vlaanderen](https://geo.api.vlaanderen.be/Landbgebrperc/wfs) - [GetCapabilities](https://geo.api.vlaanderen.be/Landbgebrperc/wfs?REQUEST=GetCapabilities&SERVICE=WFS)

* [WFS Landbouwstreken België - agentschap Digitaal Vlaanderen](https://geo.api.vlaanderen.be/Landbouwstreken/wfs) - [GetCapabilities](https://geo.api.vlaanderen.be/Landbouwstreken/wfs?REQUEST=GetCapabilities&SERVICE=WFS)

* [WFS Landinrichting - agentschap Digitaal Vlaanderen](https://geo.api.vlaanderen.be/Landinrichting/wfs) - [GetCapabilities](https://geo.api.vlaanderen.be/Landinrichting/wfs?REQUEST=GetCapabilities&SERVICE=WFS)

* [WFS Lozingspunten Richtlijn Stedelijk afvalwater - agentschap Digitaal Vlaanderen](https://geo.api.vlaanderen.be/Uwwtd-DischargePoints/wfs) - [GetCapabilities](https://geo.api.vlaanderen.be/Uwwtd-DischargePoints/wfs?REQUEST=GetCapabilities&SERVICE=WFS)

* [WFS Meetnet Afvalwater - agentschap Digitaal Vlaanderen](https://geo.api.vlaanderen.be/MeetnetAfvalwater/wfs) - [GetCapabilities](https://geo.api.vlaanderen.be/MeetnetAfvalwater/wfs?REQUEST=GetCapabilities&SERVICE=WFS)

* [WFS Meetnet Riooloverstorten - agentschap Digitaal Vlaanderen](https://geo.api.vlaanderen.be/MeetnetRiooloverstorten/wfs) - [GetCapabilities](https://geo.api.vlaanderen.be/MeetnetRiooloverstorten/wfs?REQUEST=GetCapabilities&SERVICE=WFS)

* [WFS Meetplaatsen Oppervlaktewaterkwaliteit - agentschap Digitaal Vlaanderen](https://geo.api.vlaanderen.be/MeetplOppervlwaterkwal/wfs) - [GetCapabilities](https://geo.api.vlaanderen.be/MeetplOppervlwaterkwal/wfs?REQUEST=GetCapabilities&SERVICE=WFS)

* [WFS Meetpunten in oppervlakte- en grondwaterlichamen Kaderrichtlijn water - agentschap Digitaal Vlaanderen](https://geo.api.vlaanderen.be/WFD-MonitoringSite/wfs) - [GetCapabilities](https://geo.api.vlaanderen.be/WFD-MonitoringSite/wfs?REQUEST=GetCapabilities&SERVICE=WFS)

* [WFS Natuurinrichtingsprojecten - agentschap Digitaal Vlaanderen](https://geo.api.vlaanderen.be/Natuurinrichting/wfs) - [GetCapabilities](https://geo.api.vlaanderen.be/Natuurinrichting/wfs?REQUEST=GetCapabilities&SERVICE=WFS)

* [WFS oppervlakte meetpunten Nitraatrichtlijn - agentschap Digitaal Vlaanderen](https://geo.api.vlaanderen.be/NID-SWMonitoringStations/wfs) - [GetCapabilities](https://geo.api.vlaanderen.be/NID-SWMonitoringStations/wfs?REQUEST=GetCapabilities&SERVICE=WFS)

* [WFS Oppervlaktewaterlichamen - agentschap Digitaal Vlaanderen](https://geo.api.vlaanderen.be/Oppervlaktewaterlichamen/wfs) - [GetCapabilities](https://geo.api.vlaanderen.be/Oppervlaktewaterlichamen/wfs?REQUEST=GetCapabilities&SERVICE=WFS)

* [WFS Oppervlaktewaterlichamen (centerlines) Kaderrichtlijn water - agentschap Digitaal Vlaanderen](https://geo.api.vlaanderen.be/WFD-SurfaceWaterBodyCenterline/wfs) - [GetCapabilities](https://geo.api.vlaanderen.be/WFD-SurfaceWaterBodyCenterline/wfs?REQUEST=GetCapabilities&SERVICE=WFS)

* [WFS Oppervlaktewaterlichamen (lijnen) Kaderrichtlijn water - agentschap Digitaal Vlaanderen](https://geo.api.vlaanderen.be/WFD-SurfaceWaterBodyLine/wfs) - [GetCapabilities](https://geo.api.vlaanderen.be/WFD-SurfaceWaterBodyLine/wfs?REQUEST=GetCapabilities&SERVICE=WFS)

* [WFS Oppervlaktewaterlichamen (polygonen) Kaderrichtlijn water - agentschap Digitaal Vlaanderen](https://geo.api.vlaanderen.be/WFD-SurfaceWaterBody/wfs) - [GetCapabilities](https://geo.api.vlaanderen.be/WFD-SurfaceWaterBody/wfs?REQUEST=GetCapabilities&SERVICE=WFS)

* [WFS Overstromingsgebieden en oeverzones - agentschap Digitaal Vlaanderen](https://geo.api.vlaanderen.be/OGOZ/wfs) - [GetCapabilities](https://geo.api.vlaanderen.be/OGOZ/wfs?REQUEST=GetCapabilities&SERVICE=WFS)

* [WFS Parameters woningkwaliteitsbeleid - agentschap Digitaal Vlaanderen](https://geo.api.vlaanderen.be/Woningkwaliteitbeleid/wfs) - [GetCapabilities](https://geo.api.vlaanderen.be/Woningkwaliteitbeleid/wfs?REQUEST=GetCapabilities&SERVICE=WFS)

* [WFS Percelen Vlaamse en Lokale Overheden - agentschap Digitaal Vlaanderen](https://geo.api.vlaanderen.be/PercelenVLO/wfs) - [GetCapabilities](https://geo.api.vlaanderen.be/PercelenVLO/wfs?REQUEST=GetCapabilities&SERVICE=WFS)

* [WFS Potentieel natuurlijke vegetatie - agentschap Digitaal Vlaanderen](https://geo.api.vlaanderen.be/PNV/wfs) - [GetCapabilities](https://geo.api.vlaanderen.be/PNV/wfs?REQUEST=GetCapabilities&SERVICE=WFS)

* [WFS Recent overstroomde gebieden - agentschap Digitaal Vlaanderen](https://geo.api.vlaanderen.be/ROG/wfs) - [GetCapabilities](https://geo.api.vlaanderen.be/ROG/wfs?REQUEST=GetCapabilities&SERVICE=WFS)

* [WFS Reiswegen De Lijn - agentschap Digitaal Vlaanderen](https://geo.api.vlaanderen.be/Reiswegen/wfs) - [GetCapabilities](https://geo.api.vlaanderen.be/Reiswegen/wfs?REQUEST=GetCapabilities&SERVICE=WFS)

* [WFS Rioolinventaris Vlaanderen - agentschap Digitaal Vlaanderen](https://geo.api.vlaanderen.be/Rioolinventaris/wfs) - [GetCapabilities](https://geo.api.vlaanderen.be/Rioolinventaris/wfs?REQUEST=GetCapabilities&SERVICE=WFS)

* [WFS Rioolwaterzuiveringsinstallatie Richtlijn Stedelijk afvalwater - agentschap Digitaal Vlaanderen](https://geo.api.vlaanderen.be/Uwwtd-UWWTP/wfs) - [GetCapabilities](https://geo.api.vlaanderen.be/Uwwtd-UWWTP/wfs?REQUEST=GetCapabilities&SERVICE=WFS)

* [WFS Ruilverkaveling van landeigendommen in der minne - agentschap Digitaal Vlaanderen](https://geo.api.vlaanderen.be/RvkInDerMinne/wfs) - [GetCapabilities](https://geo.api.vlaanderen.be/RvkInDerMinne/wfs?REQUEST=GetCapabilities&SERVICE=WFS)

* [WFS Ruilverkaveling van landeigendommen uit kracht van wet - agentschap Digitaal Vlaanderen](https://geo.api.vlaanderen.be/RvkKrachtWet/wfs) - [GetCapabilities](https://geo.api.vlaanderen.be/RvkKrachtWet/wfs?REQUEST=GetCapabilities&SERVICE=WFS)

* [WFS Ruilverkaveling van landeigendommen uit kracht van wet bij uitvoering van grote infrastructuurwerken - agentschap Digitaal Vlaanderen](https://geo.api.vlaanderen.be/RvkGrInfraWerken/wfs) - [GetCapabilities](https://geo.api.vlaanderen.be/RvkGrInfraWerken/wfs?REQUEST=GetCapabilities&SERVICE=WFS)

* [WFS RVV-Themabestand - agentschap Digitaal Vlaanderen](https://geo.api.vlaanderen.be/RVVThemabestand/wfs) - [GetCapabilities](https://geo.api.vlaanderen.be/RVVThemabestand/wfs?REQUEST=GetCapabilities&SERVICE=WFS)

* [WFS Statistische sectoren van België - agentschap Digitaal Vlaanderen](https://geo.api.vlaanderen.be/StatistischeSectoren/wfs) - [GetCapabilities](https://geo.api.vlaanderen.be/StatistischeSectoren/wfs?REQUEST=GetCapabilities&SERVICE=WFS)

* [WFS Steunzones - agentschap Digitaal Vlaanderen](https://geo.api.vlaanderen.be/Steunzones/wfs) - [GetCapabilities](https://geo.api.vlaanderen.be/Steunzones/wfs?REQUEST=GetCapabilities&SERVICE=WFS)

* [WFS Stroomgebiedsdistricten Kaderrichtlijn water - agentschap Digitaal Vlaanderen](https://geo.api.vlaanderen.be/WFD-RiverBasinDistrict/wfs) - [GetCapabilities](https://geo.api.vlaanderen.be/WFD-RiverBasinDistrict/wfs?REQUEST=GetCapabilities&SERVICE=WFS)

* [WFS Sub-unit Kaderrichtlijn water - agentschap Digitaal Vlaanderen](https://geo.api.vlaanderen.be/WFD-SubUnit/wfs) - [GetCapabilities](https://geo.api.vlaanderen.be/WFD-SubUnit/wfs?REQUEST=GetCapabilities&SERVICE=WFS)

* [WFS Toerisme Vlaanderen - Toerisme Vlaanderen](https://geodata.toerismevlaanderen.be/geoserver/poi/wfs) - [GetCapabilities](https://geodata.toerismevlaanderen.be/geoserver/poi/wfs?REQUEST=GetCapabilities&SERVICE=WFS)

* [WFS Traditionele landschappen - agentschap Digitaal Vlaanderen](https://geo.api.vlaanderen.be/TradLa/wfs) - [GetCapabilities](https://geo.api.vlaanderen.be/TradLa/wfs?REQUEST=GetCapabilities&SERVICE=WFS)

* [WFS Van nature overstroombare gebieden - agentschap Digitaal Vlaanderen](https://geo.api.vlaanderen.be/NOG/wfs) - [GetCapabilities](https://geo.api.vlaanderen.be/NOG/wfs?REQUEST=GetCapabilities&SERVICE=WFS)

* [WFS Verrijkte KruispuntBank Ondernemingen - agentschap Digitaal Vlaanderen](https://geo.api.vlaanderen.be/VKBO/wfs) - [GetCapabilities](https://geo.api.vlaanderen.be/VKBO/wfs?REQUEST=GetCapabilities&SERVICE=WFS)

* [WFS Vlaamse Hydrografische Atlas - Waterlopen - agentschap Digitaal Vlaanderen](https://geo.api.vlaanderen.be/VHAWaterlopen/wfs) - [GetCapabilities](https://geo.api.vlaanderen.be/VHAWaterlopen/wfs?REQUEST=GetCapabilities&SERVICE=WFS)

* [WFS Vlaamse Leveringsgebieden Drinkwater drinkwaterrichtlijn - agentschap Digitaal Vlaanderen](https://geo.api.vlaanderen.be/DWD-WaterSupplyZones/wfs) - [GetCapabilities](https://geo.api.vlaanderen.be/DWD-WaterSupplyZones/wfs?REQUEST=GetCapabilities&SERVICE=WFS)

* [WFS Vlaamse Zwemwaterlocaties - agentschap Digitaal Vlaanderen](https://geo.api.vlaanderen.be/VlaamseZwemwaterlocaties/wfs) - [GetCapabilities](https://geo.api.vlaanderen.be/VlaamseZwemwaterlocaties/wfs?REQUEST=GetCapabilities&SERVICE=WFS)

* [WFS Voorlopig referentiebestand gemeentegrenzen - agentschap Digitaal Vlaanderen](https://geo.api.vlaanderen.be/VRBG/wfs) - [GetCapabilities](https://geo.api.vlaanderen.be/VRBG/wfs?REQUEST=GetCapabilities&SERVICE=WFS)

* [WFS Voorlopig referentiebestand gemeentegrenzen 2003 - agentschap Digitaal Vlaanderen](https://geo.api.vlaanderen.be/VRBG2003/wfs) - [GetCapabilities](https://geo.api.vlaanderen.be/VRBG2003/wfs?REQUEST=GetCapabilities&SERVICE=WFS)

* [WFS Voorlopig referentiebestand gemeentegrenzen 2019 - agentschap Digitaal Vlaanderen](https://geo.api.vlaanderen.be/VRBG2019/wfs) - [GetCapabilities](https://geo.api.vlaanderen.be/VRBG2019/wfs?REQUEST=GetCapabilities&SERVICE=WFS)

* [WFS Voorlopig referentiebestand gemeentegrenzen 2025 - agentschap Digitaal Vlaanderen](https://geo.api.vlaanderen.be/VRBG2025/wfs) - [GetCapabilities](https://geo.api.vlaanderen.be/VRBG2025/wfs?REQUEST=GetCapabilities&SERVICE=WFS)

* [WFS Waterkwaliteitsdoelstellingen Wateroppervlakken - agentschap Digitaal Vlaanderen](https://geo.api.vlaanderen.be/WokwdoelWateropp/wfs) - [GetCapabilities](https://geo.api.vlaanderen.be/WokwdoelWateropp/wfs?REQUEST=GetCapabilities&SERVICE=WFS)

* [WFS Watersystemen - agentschap Digitaal Vlaanderen](https://geo.api.vlaanderen.be/Watersystemen/wfs) - [GetCapabilities](https://geo.api.vlaanderen.be/Watersystemen/wfs?REQUEST=GetCapabilities&SERVICE=WFS)

* [WFS Wegenregister - agentschap Digitaal Vlaanderen](https://geo.api.vlaanderen.be/Wegenregister/wfs) - [GetCapabilities](https://geo.api.vlaanderen.be/Wegenregister/wfs?REQUEST=GetCapabilities&SERVICE=WFS)

* [WFS Werkingsgebieden van wildbeheereenheden - agentschap Digitaal Vlaanderen](https://geo.api.vlaanderen.be/Wbe/wfs) - [GetCapabilities](https://geo.api.vlaanderen.be/Wbe/wfs?REQUEST=GetCapabilities&SERVICE=WFS)

* [WFS Woningbouw- en woonvernieuwingsgebieden - agentschap Digitaal Vlaanderen](https://geo.api.vlaanderen.be/WoningbWoonvernieuwing/wfs) - [GetCapabilities](https://geo.api.vlaanderen.be/WoningbWoonvernieuwing/wfs?REQUEST=GetCapabilities&SERVICE=WFS)



## Wallonia

* [APP_IRIS_IRIS_DS_WFS - geoservices2.wallonie.be](https://geoservices2.wallonie.be/arcgis/services/APP_IRIS/IRIS_DS_WFS/MapServer/WFSServer) - [GetCapabilities](https://geoservices2.wallonie.be/arcgis/services/APP_IRIS/IRIS_DS_WFS/MapServer/WFSServer?REQUEST=GetCapabilities&SERVICE=WFS)

* [APP_IRIS_IRIS_LAST_WFS - geoservices2.wallonie.be](https://geoservices2.wallonie.be/arcgis/services/APP_IRIS/IRIS_LAST_WFS/MapServer/WFSServer) - [GetCapabilities](https://geoservices2.wallonie.be/arcgis/services/APP_IRIS/IRIS_LAST_WFS/MapServer/WFSServer?REQUEST=GetCapabilities&SERVICE=WFS)

* [WFS - Service public de Wallonie](https://geoservices.wallonie.be/arcgis/services/WFS/SIGEC_PARC_AGRI_ANON/MapServer/WFSServer) - [GetCapabilities](https://geoservices.wallonie.be/arcgis/services/WFS/SIGEC_PARC_AGRI_ANON/MapServer/WFSServer?REQUEST=GetCapabilities&SERVICE=WFS)



## World

* [Flanders Marine Institute (VLIZ) - WFS Service - VLIZ](https://geo.vliz.be/geoserver/MarineRegions/wfs) - [GetCapabilities](https://geo.vliz.be/geoserver/MarineRegions/wfs?REQUEST=GetCapabilities&SERVICE=WFS)

* [WFS - image.discomap.eea.europa.eu](https://image.discomap.eea.europa.eu/arcgis/services/EUHydro/RiverBasine/MapServer/WFSServer) - [GetCapabilities](https://image.discomap.eea.europa.eu/arcgis/services/EUHydro/RiverBasine/MapServer/WFSServer?REQUEST=GetCapabilities&SERVICE=WFS)

* [WFS - image.discomap.eea.europa.eu](https://image.discomap.eea.europa.eu/arcgis/services/GioLand/2012_Coverage1_20m/MapServer/WFSServer) - [GetCapabilities](https://image.discomap.eea.europa.eu/arcgis/services/GioLand/2012_Coverage1_20m/MapServer/WFSServer?REQUEST=GetCapabilities&SERVICE=WFS)

* [WFS - image.discomap.eea.europa.eu](https://image.discomap.eea.europa.eu/arcgis/services/GioLandPublic/Lucas2012/MapServer/WFSServer) - [GetCapabilities](https://image.discomap.eea.europa.eu/arcgis/services/GioLandPublic/Lucas2012/MapServer/WFSServer?REQUEST=GetCapabilities&SERVICE=WFS)

* [WFS - sampleserver6.arcgisonline.com](https://sampleserver6.arcgisonline.com/arcgis/services/SampleWorldCities/MapServer/WFSServer) - [GetCapabilities](https://sampleserver6.arcgisonline.com/arcgis/services/SampleWorldCities/MapServer/WFSServer?REQUEST=GetCapabilities&SERVICE=WFS)

* [WMS Demo Server for MapServer - GatewayGeo](https://demo.mapserver.org/cgi-bin/wfs) - [GetCapabilities](https://demo.mapserver.org/cgi-bin/wfs?REQUEST=GetCapabilities&SERVICE=WFS)

